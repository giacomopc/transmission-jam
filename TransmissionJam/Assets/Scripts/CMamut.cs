﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class CMamut : MonoBehaviour {

	public Transform mamut;
	public Text text;

	public Material[] materials;
	public Renderer[] mammothParts;
	public Material matWhite;

	void Start () 
	{
		StartCoroutine (Animation ());
		text.color = Color.clear;
		mamut.localPosition = new Vector3 (0f, -1.41f, 900f);
	}

	IEnumerator Animation()
	{
		yield return new WaitForSeconds (1f);

		const float Duration1 = 3f;
		mamut.DOLocalMoveZ (0f, Duration1, false);
		mamut.DOLocalRotate (Vector3.up * 360f * 10f, Duration1, RotateMode.FastBeyond360).SetEase(Ease.OutBack);

		yield return new WaitForSeconds(Duration1 + 2f);

		const float Duration2 = 1f;
		mamut.DOLocalRotate (Vector3.up * 90f, Duration2);
		text.DOColor (Color.black, Duration2);

		yield return new WaitForSeconds(Duration2 + 0.5f);

		const float Duration3 = 1f;
		Camera.main.DOColor (new Color(0.2f, 0.2f, 0.2f, 1f), Duration3);
		text.DOColor (Color.white, Duration3);

		yield return new WaitForSeconds (Duration3 + 2f);
		SceneManager.LoadScene ("home1");
	}

}
