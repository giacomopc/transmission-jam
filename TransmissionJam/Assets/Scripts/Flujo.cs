﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flujo : MonoBehaviour 
{

	// Unity connections

	public GameObject startButton;
	public GameObject playerSelect;


	// main menu screen
	public GameObject screen_mainMenu;
	public GameObject screen_recordMessage;
	public GameObject screen_playbackAndRecord;
	public GameObject screen_passIt;
	public GameObject screen_pickImage;

	// record message
	public GameObject recordMessage_recordButton;
	public GameObject recordMessage_stopButton;

	// playback and record
	public GameObject playbackAndRecord_recordButton;
	public GameObject playbackAndRecord_stopButton;
	public GameObject playbackAndRecord_listenButton;
	public Text playbackAndRecord_label;

	// pass it
	public Text passIt_label;

	// pick image
	public Image pickImage_currentImage;
	public int currentImageIndex;
	public Sprite[] pickImage_sprites;


	// pick image
	public Sprite initialSprite;
	public int initialSpriteIndex;
	public string prefix;


	public Controller recordController;

	public GameObject ResultUI;
	public Text resultMessage, resultDescription;
	public Image successImage, resultFrontImage, resultBackImage;
	bool isShowingInitial = false;

	public bool isTime = false;
	public Text time;
	public Image imageToShow, resultImage;
	public Sprite[] spriteArray;

	// load sprites
	const int AlternateImages = 3;
	const int recordLength = 10;
	public float timeLeft = 10.0f;

	public string[] imageSets;

    void Start () 
	{
		screen_mainMenu.SetActive (true);
		screen_playbackAndRecord.SetActive (false);
		screen_recordMessage.SetActive (false);
		screen_passIt.SetActive (false);
		screen_pickImage.SetActive (false);
    }

	public void ResetTurns()
	{
		recordController.RestartGame ();
	}

	public void AddTurn()
	{
		recordController.turn = 0;
	}

    public void PlayDistortionRec()
  	{
		print ("play distortion rec");
		recordController.Listen ();
    }


    public void RecordSound()
    {
		recordController.Record ();

		if (recordController.turn == 0)
		{
			recordMessage_recordButton.SetActive (false);
			recordMessage_stopButton.SetActive (true);
		}
		else
		{
			playbackAndRecord_recordButton.SetActive(false);
			playbackAndRecord_stopButton.SetActive(true);
		}
    }
		
    public void StopRecord()
    {
		//if(recordController)
		recordController.StopRecording ();

		if (recordController.turn == 0)
		{
			screen_recordMessage.SetActive (false);
		}
		else
		{
			screen_playbackAndRecord.SetActive (false);
		}

		screen_passIt.SetActive (true);
    }

	public void UpdatePickImageImage()
	{
		// set first image from the pack
		pickImage_currentImage.sprite = pickImage_sprites [currentImageIndex];	
		pickImage_currentImage.preserveAspect = true;
	}

	public void NextPlayerButton()
	{
		screen_passIt.SetActive (false);

		if (!recordController.NextTurn ())
		{
			// LAST TURN
			//print ("last turn");
			screen_pickImage.SetActive (true);

			print("setrandomimage sprite name: " + imageToShow.sprite.name);
			imageToShow.preserveAspect = true;

			pickImage_sprites = new Sprite[AlternateImages];

			for (var i = 0; i < AlternateImages; i++)
			{
				var imageName = prefix + i;
				pickImage_sprites [i] = Resources.Load<Sprite> (imageName);
			}

			currentImageIndex = 0;

			UpdatePickImageImage ();
		}
		else
		{
			//print ("turn: " + recordController.turn);
			screen_playbackAndRecord.SetActive (true);

			playbackAndRecord_recordButton.SetActive (true);
			playbackAndRecord_stopButton.SetActive (false);

			var playerNumber = (recordController.turn + 1);
			playbackAndRecord_label.text = 
				"<color=red>Player " + 
				playerNumber + 
				"</color>, listen to the audio and record it as clearly as possible.";
		}
	}

//    public void StartTime()
//    {
//        timeLeft = 10.0f;
//		listenBtn.SetActive(true);
//		Rec_btn.SetActive(false);
//        isTime = true;
//    }
    

	public void SwapImageForward()
	{
		currentImageIndex = (currentImageIndex + 1) % AlternateImages;
		UpdatePickImageImage ();

	}

	public void SwapImageBackwards()
	{
		currentImageIndex = currentImageIndex - 1;
		if (currentImageIndex == -1)
		{
			currentImageIndex = pickImage_sprites.Length - 1;
		}

		UpdatePickImageImage ();
	}

	public void ShowResultUI(){
		screen_pickImage.SetActive (false);
		if (pickImage_sprites[currentImageIndex] == initialSprite)
		{
			resultFrontImage.gameObject.SetActive (false);
			resultBackImage.gameObject.SetActive (false);
			successImage.gameObject.SetActive (true);
			successImage.sprite = initialSprite;
			resultMessage.text = "<color=#1ace6b>SUCCESS</color>";
		}
		else
		{
			resultFrontImage.gameObject.SetActive (true);
			resultBackImage.gameObject.SetActive (true);
			successImage.gameObject.SetActive (false);
			resultFrontImage.sprite = initialSprite;
			resultBackImage.sprite = pickImage_sprites [currentImageIndex];
			resultMessage.text = "<color=red>FAILED</color>";
			resultDescription.text = wrongChoice;
			isShowingInitial = true;
		}
		ResultUI.gameObject.SetActive (true);
	}

	public void PlayAgainButton()
	{
		recordMessage_recordButton.SetActive (true);
		ResultUI.SetActive (false);
		SelectedPlayers (recordController.playerCount);
	}

	public void ExitButton()
	{
		screen_mainMenu.SetActive (true);
		ResultUI.SetActive (false);
	}

	string wrongChoice = "Your <color=red>(wrong)</color> choice";
	public void SwapResultImages()
	{
//		Sprite temp = resultBackImage.sprite;
//		resultBackImage.sprite = resultFrontImage.sprite;
//		resultFrontImage.sprite = temp;
		isShowingInitial = !isShowingInitial;
		if (isShowingInitial)
		{
			resultFrontImage.sprite = initialSprite;
			resultBackImage.sprite = pickImage_sprites [currentImageIndex];
			resultDescription.text = wrongChoice;
		}
		else
		{
			resultFrontImage.sprite = pickImage_sprites [currentImageIndex];
			resultBackImage.sprite = initialSprite;
			resultDescription.text = "Your image";
		}
	}

	public void StartButton()
	{
		print ("start button");
		playerSelect.gameObject.SetActive (true);
		startButton.gameObject.SetActive (false);
	}

	// called by unity
	public void SelectedPlayers(int players)
	{
		print ("selected players: " + players);
		recordController.turn = 0;
		//playerSelect.gameObject.SetActive (false);
		screen_mainMenu.SetActive (false);
		screen_recordMessage.SetActive (true);

		recordController.playerCount = players;

		var randomImage = Random.Range(0, spriteArray.Length);
		imageToShow.sprite = spriteArray[randomImage];

		initialSprite = imageToShow.sprite;
		prefix = initialSprite.name.Substring (0, initialSprite.name.Length - 1);
		imageToShow.preserveAspect = true;
	}

	IEnumerator FadeOut()
	{
		yield return null;	
	}

	IEnumerator FadeIn()
	{
		yield return null;
	}

	public void ShowResult()
	{
		ResultUI.gameObject.SetActive (true);
		resultImage.sprite = spriteArray [currentImageIndex];
		resultMessage.text = (currentImageIndex == initialSpriteIndex) ?
			"Success" :
			"Failed" ;

		print ("result: " + resultMessage.text);

		//Pick_Img.gameObject.SetActive (false);
	}
 }
