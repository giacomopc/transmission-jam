﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AnimateFrames : MonoBehaviour 
{
	public Sprite[] sprites;
	public float fps;

	void Update () 
	{
		var frame = Mathf.FloorToInt (Time.timeSinceLevelLoad * fps % sprites.Length); 
		GetComponent<Image> ().sprite = sprites [frame];	
	}
}
