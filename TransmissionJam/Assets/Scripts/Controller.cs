﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour 
{
	enum RecordingState
	{
		HasntRecorded,
		Recording,
		Finished
	}

	// constants	
	//const int Turns = 4;
	const int MaxTurns = 10;
	const float MaxRecordTime = 10f;
	const string DeviceName = "Internal Microphone";
	const int SoundHz = 44100;

	// public variables
	public AudioClip pinkNoise;
	public AudioSource pinkNoiseSource;

	// private variables
	public int turn = 0;
	public int playerCount = 0;

	//AudioSource currentAudioSource;

	AudioSource[] audioSources = new AudioSource[MaxTurns];
	AudioClip[] recordedClips = new AudioClip[MaxTurns];
	float[] timeRecorded = new float[MaxTurns];

	float timeStartedRecording = 0f;

	void Start()
	{
		for (int i = 0; i < MaxTurns; i++)
		{
			audioSources[i] = (new GameObject ("AudioSource")).AddComponent<AudioSource> ();
		}

		RestartGame ();
		//status.text = "Presiona <b>espacio</b> para comenzar a grabar.";
	}


	public bool NextTurn()
	{
		//print ("next turn"); 
		turn++;

		print ("turn: " + turn + "// playerCount: " + playerCount);

		return (turn != playerCount - 1);
	}
	public void Record ()
	{
		// Starts recording
		//status.text = "Grabando...";

		print ("recording");

		recordedClips [turn] = Microphone.Start (
			DeviceName, 
			false, 
			Mathf.FloorToInt (MaxRecordTime), 
			SoundHz
		);	

		timeStartedRecording = Time.timeSinceLevelLoad;
	
	}

	public void StopRecording()
	{
		timeRecorded[turn] = Time.timeSinceLevelLoad - timeStartedRecording;

		if (Microphone.IsRecording (DeviceName))
		{
			Microphone.End (DeviceName);
		}

	}

	public void OnGUI()
	{
		var rect = new Rect (0, 0, 100, 100);
		GUI.Label (rect, "recordedClip != null: " + (recordedClips[turn] != null).ToString ());
	}


	void EffectPitch(AudioSource source)
	{
		source.pitch = 1.5f;
		source.volume = 1f;
	}

	void EffectCut(AudioSource source)
	{
		source.volume = 4f;
		source.pitch = 1f;
	}

	void EffectNoise(float endTime)
	{
		pinkNoiseSource.volume = 0.24f;
		pinkNoiseSource.Play ();
		pinkNoiseSource.SetScheduledEndTime (endTime);
	}

	void AddEffectToAudioSource(AudioSource audioSource)
	{
			
	}

	public void RestartGame()
	{
		turn = 0;
	}

	public void Listen()
	{
		print ("listen turns: " + turn);	

		var longestTime = 0f;

		for (var i = 0; i < turn; i++)
		{
			var audioSource = audioSources [i];
			audioSource.clip = recordedClips [i];
			audioSource.volume = (i == turn - 1) ? 1f : (0.3f / turn) ;
			audioSource.Play ();

			var endTime = AudioSettings.dspTime + timeRecorded [i];
			audioSource.SetScheduledEndTime (endTime);
			print ("playing: " + i);

			longestTime = Mathf.Max ((float)endTime, longestTime);
		}

		print ("longest time: " + longestTime);
		EffectNoise (longestTime);


	}

}
