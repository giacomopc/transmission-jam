﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tiling : MonoBehaviour {

	public Vector2 speed;

	RectTransform rt;

	void Start () 
	{
		rt = GetComponent<RectTransform> ();
	}

	void Update () 
	{
		var sprite = GetComponent<Image> ().sprite;

		var position = rt.anchoredPosition;
		position.x = (position.x + speed.x * Time.deltaTime) % sprite.texture.width;
		position.y = (position.y + speed.y * Time.deltaTime) % sprite.texture.height;
		rt.anchoredPosition = position;
	}
}
